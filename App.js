import React from 'react';

import { createStore } from "redux"
import { Provider } from "react-redux"
import reducer from "./reducers"

import Home from './src/screens/home'

const store = createStore(reducer)
export default function App() {
 
  return (
    <Provider store={store}>
        <Home/>
    </Provider>
  );
}

