import React from "react"
import { StyleSheet,Dimensions, Animated, View, Text } from "react-native"
import { hidden } from "ansi-colors";
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

const AnimatedContainer = Animated.createAnimatedComponent(View)

class SlideView extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            top:new Animated.Value(screenHeight),
            show:this.props.show
        }
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.show !== this.props.show){
            this.setState({
                show:nextProps.show
            })
        }
    }
    toggleModal = (value) => {
        Animated.spring(this.state.top, {
            toValue: value,
            useNativeDriver:true
        }).start()
    }
    render() {
        if(this.state.show){
            //Show
            this.toggleModal(0)
        }else{
            //Hide
            this.toggleModal(screenHeight)
        }
        let containerStyle = styles.container;
        if(this.props.style){
            containerStyle = {
                ...containerStyle,
                ...this.props.style
            }
        }
        return (
            <AnimatedContainer style={{...containerStyle, transform: [{
                translateY: this.state.top
              }]}}>
               {this.props.body}
            </AnimatedContainer>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        width:'100%',
        overflow:'hidden'
    },
    
});

export default SlideView