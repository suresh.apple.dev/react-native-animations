import React from "react"
import { StyleSheet,Dimensions, Animated, View, Text } from "react-native"
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

const AnimatedContainer = Animated.createAnimatedComponent(View)

class FadeView extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            top:new Animated.Value(screenHeight),
            show:this.props.show,
            opacity:new Animated.Value(0)
        }
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.show !== this.props.show){
            this.setState({
                show:nextProps.show
            })
        }
    }
    toggleModal = (topValue, opacity) => {
        Animated.parallel([
            Animated.timing(this.state.opacity, {
                toValue: opacity,
                delay:50,
                duration: 500,
                useNativeDriver:true
            }),
            Animated.spring(this.state.top, {
                toValue: topValue,
                delay:100,
                useNativeDriver:true
            })
        ]).start()

        // Animated.timing(this.state.opacity, {
        //     toValue: opacity,
        //     duration: 2000
        // })

        // Animated.spring(this.state.top, {
        //     toValue: topValue,
        //     delay:0,
        //     useNativeDriver:true
        // }).start()
    }
    render() {
        if(this.state.show){
            //Show
            this.toggleModal(0, 1)
        }else{
            //Hide
            this.toggleModal(100, 0)
        }
        let containerStyle = styles.container;
        if(this.props.style){
            containerStyle = {
                ...containerStyle,
                ...this.props.style
            }
        }
        return (
            <AnimatedContainer style={{...containerStyle,transform: [{
                translateY: this.state.top
              }], opacity:this.state.opacity}}>
               {this.props.body}
            </AnimatedContainer>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'transparent',
        width:'100%',
        overflow:'hidden'
    },
    
});

export default FadeView