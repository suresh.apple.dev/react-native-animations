import React from "react"
import { StyleSheet, View, Text } from "react-native"

class ModalHeader extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.titleView}>
                    <Text style={styles.titleLabel}> {this.props.title} </Text>
                </View>
                <View style={styles.buttonsContainer}>
                    <View style={styles.leftItem}>
                        {this.props.leftItem}
                    </View>
                    <View style={styles.rightItem}>
                        {this.props.rightItem}
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent:'center',
        alignItems:'center',
        backgroundColor: '#fff',
        height: 44,
    },
    buttonsContainer: {
        flex: 1,
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        backgroundColor: 'transparent',
        width: '100%',
    },
    leftItem: {
        marginLeft: 10,
        alignItems:'center',
        justifyContent:'center',
        padding:5,
    },
    rightItem: {
        marginRight: 10,
        alignItems:'center',
        justifyContent:'center',
        padding:5,
    },
    titleView: {
        flex: 1,
        position:'absolute',
        alignItems:'center',
        justifyContent:'center',
        backgroundColor: 'transparent',
    },
    titleLabel: {
       fontSize:16,
       fontWeight:'500'
    }
});

export default ModalHeader