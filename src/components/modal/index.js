import React from "react"
import { StyleSheet, Animated, Dimensions, View, Text } from "react-native"
import ModalHeader from './header';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
const AnimatedContainer = Animated.createAnimatedComponent(View)

class Modal extends React.Component {
    constructor(props){
        super(props);
        let modalHeight = this.props.height;
        let initialHeight = screenHeight;
        if(this.props.showModal){
            initialHeight = screenHeight - modalHeight;
        }
        this.state = {
            top:new Animated.Value(initialHeight)
        } 
    }
    componentDidMount(){
        this.updateView(this.props.showModal);
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.showModal !== this.props.showModal){
            this.updateView(nextProps.showModal);
        }
    }
    updateView = (showModal) => {
        if(showModal){
            this.show();
        }else{
            this.hide();
        }
    }
    show = () => {
        let modalHeight = this.props.height;
        this.toggleModal(screenHeight - modalHeight)
    }
    hide = () => {
        let modalHeight = this.props.height;
        this.toggleModal(screenHeight)
    }
    toggleModal = (value) => {
        Animated.spring(this.state.top, {
            toValue: value,
            useNativeDriver:true
        }).start()
    }
    render() {
        return (
            <AnimatedContainer style={{...styles.container, transform: [{
                translateY: this.state.top
              }]}}>
                 {
                     (this.props.showHeader)?<ModalHeader 
                     title={this.props.title}
                     leftItem={this.props.headerLeftItem}
                     rightItem={this.props.headerRightItem}/>:null
                 } 
                <View style={styles.body}>
                    {this.props.body}
                </View>
            </AnimatedContainer>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        backgroundColor: '#fff',
        width:'100%',
        height:'100%',
        zIndex:100,
        borderTopLeftRadius:20,
        borderTopRightRadius:20,
        overflow:'hidden'
    },
    body:{
        backgroundColor: '#fff',
        flex: 1,
    }
});

export default Modal