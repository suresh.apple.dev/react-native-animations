import React from 'react';
import { Text, View } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
// import { createSharedElementStackNavigator } from 'react-navigation-shared-element';

import Dashboard from '../dashboard';
import Community from '../community';
import CommunityDetails from '../community/details';
import Finance from '../finance';
import More from '../more';



const TabNavigator = createBottomTabNavigator({
  Community: Community,
  Dashboard: Dashboard,
  Finance:Finance,
  More:More
});

export default createAppContainer(TabNavigator);