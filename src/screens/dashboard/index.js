import React from "react"
import { StyleSheet, Dimensions, SafeAreaView, Animated, ScrollView, View, Text, Button, ImageBackground, TouchableOpacity, LinearGradient } from "react-native"
import Modal from '../../components/modal';
import SlideView from '../../components/slide-view';
import FadeView from '../../components/fade-view';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

let contentViewHeight = 1000;
let navBarHeight = 80
let topViewOrigin = 400
let topViewHeight = screenHeight - topViewOrigin

export default class Dashboard extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            showModal: false,
            showOverview: true,
            showContentView: true,
            scrollOffsetY: new Animated.Value(0)
        }

        // this.scrollOffsetY = new Animated.Value(0);
        // this.scrollOffsetY.addListener(Animated.event([{value: this.scroll}], {useNativeDriver: false}));
        this.state.scrollOffsetY.addListener(({value}) => this.scrollOffsetY = value);
    }
    componentDidMount(){
        // let self = this;
        // setTimeout(() => {
        //     // This will scroll the view to SubView2
        //     self.scrollView.getNode().scrollTo({x: 0, y: 100, animated: true})
        // }, 5000);
    }
    cancelModalView = () => {
        this.setState({
            showModal:false,
            showOverview:true,
            showContentView:true,
            scrollOffsetY: new Animated.Value(0)
        })
    }
    showModalView = () => {
        this.setState({
            showModal:true,
            showOverview:false,
            showContentView:false
        })
    }
    showContentView = () => {
        // this.scrollOffsetY = new Animated.Value(0);
        // console.log("this.scrollView : "+this.scrollView)
        // this.scrollView.contentOffset.y = this.scrollOffsetY
        // this.scrollView.scrollTo({ y: 0, animated: true });
        this.scrollView.contentOffset = {x:0, y:0}
        this.scrollView.scrollsToTop = true;
        this.scrollView.getNode().scrollTo({x: 0, y: 0, animated: true})
        this.setState({
            showModal:false,
            showOverview:true,
            showContentView:true,
            scrollOffsetY: new Animated.Value(0)
        })
        
        // this.setState({
        //     showModal:false,
        //     showOverview:true,
        //     showContentView:true,
        //     scrollOffsetY: new Animated.Value(0)
        // })
        
        // alert("this.scrollOffsetY : "+this.scrollOffsetY)
    }
    animateShowContentView = () => {
        Animated.parallel([
            Animated.spring(this.state.scrollOffsetY, {
                toValue: 0,
                delay:100,
                useNativeDriver:true
            })
        ]).start()
    }
    refScrollView = (scrollView) => {
        this.scrollView = scrollView;
    }
    handleScroll = (event) => {
        console.log(event.nativeEvent.contentOffset.y);
    }
    getOverView = () => {
        return (<View style={styles.overView}>
                    <Text style={styles.titleText}>Overview</Text>
                    <TouchableOpacity onPress={this.showModalView} style={styles.button}>
                        <Text style={styles.buttonTitle}>OPEN MODAL</Text>
                    </TouchableOpacity>
                </View>)
    }
    render() {
        if(this.state.showContentView){
            this.animateShowContentView();
        }
        let imgOpacity = this.state.scrollOffsetY.interpolate({
            inputRange: [0, screenHeight/2],
            outputRange: [1, 0],
        });
        let navBarOrigin = this.state.scrollOffsetY.interpolate({
            inputRange: [topViewOrigin - (3*navBarHeight), topViewOrigin - (2*navBarHeight)],
            outputRange: [0, 1],
        });

        
        return (
            // <SafeAreaView style={styles.safeArea}>
                    <View style={styles.container}>

                        <Modal 
                            title="Header" 
                            headerLeftItem={<Button onPress={this.cancelModalView} title="Cancel"/>}
                            headerRightItem={<Button onPress={this.cancelModalView} title="Apply"/>}
                            // body={<Text>BODY CONTENT</Text>}
                            height={screenHeight - 200}
                            showHeader={true}
                            showModal={this.state.showModal}/>
                            
                            <Animated.View style={{...styles.navBar, opacity:navBarOrigin}}>
                                <TouchableOpacity onPress={this.showContentView}><Text style={{color:'#000'}}>Back</Text></TouchableOpacity>
                            </Animated.View>

                            <Animated.ScrollView 
                            // onScroll={this.handleScroll}
                            scrollEventThrottle={5}
                            onScroll={Animated.event([{nativeEvent: {contentOffset: {y: this.state.scrollOffsetY}}}], {useNativeDriver: true})}
                            bounces={false}
                            ref={this.refScrollView}
                            style={{backgroundColor:'#fff'}}
                            >
                                <View style={styles.topView}>
                                    <Animated.Image
                                        source={require('../../../assets/bg-overview.jpg')}
                                        style={{height: screenHeight, width: "100%", opacity: imgOpacity, position:'absolute', transform: [{translateY: Animated.multiply(this.state.scrollOffsetY, 0.65)}]}}>
                                    </Animated.Image>
                                    {/* <LinearGradient
                                        colors={["rgba(255,255,255,0.9)", "rgba(255,255,255,0.35)", "rgba(255,255,255,0)"]}
                                        locations={[0, 0.25, 1]}
                                        style={{position: "absolute", height: "100%", width: "100%"}}/> */}
                                    <FadeView
                                        show={this.state.showOverview}
                                        body={this.getOverView()}/>
                                </View>
                                <View style={styles.contentView}>
                                    <SlideView 
                                        show={this.state.showContentView}
                                        body={<View style={{alignItems:'center', marginTop:10}}><Text>SLIDE VIEW</Text></View>}
                                        style={{borderTopLeftRadius:20, borderTopRightRadius:20}}
                                        />
                                </View>
                            </Animated.ScrollView>
                    </View>
               
            // </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    safeArea: {
        flex: 1,
        marginTop: 0,
    },
    container: {
        flex: 1,
        backgroundColor: '#000',
        resizeMode:'stretch'
    },
    navBar: {
        position:'absolute',
        height:navBarHeight,
        top:0,
        width:screenWidth,
        justifyContent:'center',
        alignItems:'center',
        padding:10,
        paddingTop:40,
        backgroundColor:'#fff',
        zIndex:100,
        shadowOpacity: 0.75,
        shadowRadius: 5,
        shadowColor: '#ccc',
        shadowOffset: { height: 0, width: 0 },
    },
    topView: {
        // flex: 1,
        height:topViewHeight,
        width:screenWidth,
    },
    overView: {
        flex: 1,
        width:'100%',
        backgroundColor: 'transparent',
        alignItems: 'flex-start',
        justifyContent: 'flex-end',
        padding:20,
        resizeMode:'stretch'
    },
    contentView: {
        height:contentViewHeight,
        width:screenWidth,
        backgroundColor:'transparent'
    },
    titleText:{
        color:'#fff',
        fontSize:35,
        fontWeight:'500'
    },
    button:{
        marginTop:10,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical:7,
        paddingHorizontal:10,
        borderRadius:5,
        backgroundColor:'#fff'
    },
    buttonTitle:{
        color:'#64043c',
        fontSize:14
    },

  });
  


