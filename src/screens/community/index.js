import React from "react"
import {
  Animated,
  Dimensions,
  View,
  Text,
  StyleSheet,
  Easing,
  TouchableOpacity,
  TouchableWithoutFeedback
} from 'react-native';
import PropTypes from 'prop-types';
import DetailScreen from './details2'
import Transition from './transition'
// import { TouchableOpacity } from "react-native-gesture-handler";

const DeviceHeight = Dimensions.get('window').height;


let cards = [
  {
    id: '1',
    title: 'Heads or Tails?',
    source: {
      uri: 'https://farm5.staticflickr.com/4386/35577308923_6f32f16374_z.jpg',
      cache: 'force-cache'
    }
  },
  {
    id: '2',
    title: 'Ghost Express',
    source: {
      uri: 'https://farm5.staticflickr.com/4374/36390435575_7e51b26c00_z.jpg',
      cache: 'force-cache'
    }
  },
  {
    id: '3',
    title: 'Heideblauwtje - Silver-studded blue',
    source: {
      uri: 'https://farm5.staticflickr.com/4411/36221070302_666eccbfbf_z.jpg',
      cache: 'force-cache'
    }
  }]

class CardView extends React.Component {
    state = {
      opacity: 1
    };
  
    static contextTypes = {
      onImageRef: PropTypes.func
    };
  
    setOpacity = opacity => {
      this.setState({ opacity });
    };
  
    render() {
      const { style, photo } = this.props;
      const { opacity } = this.state;
      return (
        <Animated.Image
          ref={i => {
            this.context.onImageRef(photo, i, this.setOpacity);
          }}
          style={[
            style,
            {
              opacity,
            //   backgroundColor:'#333',
              width:'100%',
              height:'100%',
              borderRadius: 10
            }
          ]}
          source={photo.source}
        />
      );
    }
  }

export default class Community extends React.Component {
  state = {
    card1ScrollY: new Animated.Value(DeviceHeight),
    card2ScrollY: new Animated.Value(DeviceHeight),
    card3ScrollY: new Animated.Value(DeviceHeight),
    isCardSelected:false,
    selectedCard: 0, 
    // xOffset:0, 
    // yOffset:0,
    // cardWidth:0,
    photo: cards[0],
    openProgress: new Animated.Value(0),
    isAnimating: false
  }
  _images = {};

  _imageOpacitySetters = {};

  static childContextTypes = {
    onImageRef: PropTypes.func
  };

  getChildContext() {
    return { onImageRef: this._onImageRef };
  }

  _onImageRef = (photo, imageRef, setOpacity) => {
      console.log("_onImageRef : "+photo)
      this._images[photo.id] = imageRef;
      this._imageOpacitySetters[photo.id] = setOpacity;
  };

  open = photo => {
      console.log("photo : "+photo)
      this._imageOpacitySetters[photo.id](
        this.state.openProgress.interpolate({
          inputRange: [0.005, 0.01],
          outputRange: [1, 0]
        })
      );
      this.setState({ photo, isAnimating: true }, () => {
        Animated.timing(this.state.openProgress, {
          toValue: 1,
          duration: 300,
          useNativeDriver: true
        }).start(() => {
          this.setState({ isAnimating: false });
        });
      });
  };

  close = photoId => {
    this.setState({ photo: null, isAnimating: true }, () => {
      Animated.timing(this.state.openProgress, {
        toValue: 0,
        duration: 300,
        useNativeDriver: true
      }).start(() => {
        this._imageOpacitySetters[photoId](1);
        this.setState({ isAnimating: false });
      });
    });
  };

  componentDidMount(){
    this.showCards()
  }
  showCards = () => {
    Animated.parallel([
        Animated.spring(this.state.card1ScrollY, {
            toValue: 0,
            delay:100,
            useNativeDriver:true
        }),
        Animated.spring(this.state.card2ScrollY, {
            toValue: 0,
            delay:150,
            useNativeDriver:true
        }),
        Animated.spring(this.state.card3ScrollY, {
            toValue: 0,
            delay:200,
            useNativeDriver:true
        })
    ]).start()
  }
  cardTapped = (id) => {
    // this.container.measure((fx, fy, width, height, px, py) => {
    //     console.log("px : "+px+" >>> py: "+py);
    //     // this.props.selectCard(px, py, this.props.id);
    //     this.setState({selectedCard: id, xOffset:20, yOffset:100, isCardSelected:true, cardWidth:width})
    // })

  }
  render() {
    let { card1ScrollY, card2ScrollY, card3ScrollY, photo, openProgress, isAnimating } = this.state;
    return (
      <View style={styles.container} ref={container => (this.container = container)}>
            <View style={styles.headerView}>
                <Text style={styles.titleText}>My Community</Text>
            </View>
            <Animated.View style={{...styles.card1, transform: [{
                    translateY: card1ScrollY
                }]}}>
                    <TouchableOpacity onPress={()=>this.open(photo)} style={{backgroundColor:'transparent', alignItems:'center', width:'100%', height:'100%'}}>
                        <CardView photo={photo}/>
                    </TouchableOpacity>
            </Animated.View>
            <Animated.View style={{...styles.card2, transform: [{
                    translateY: card2ScrollY
                }]}}>
                    <TouchableOpacity onPress={() => this.cardTapped(2)} style={{backgroundColor:'transparent', alignItems:'center', width:'100%', height:'100%', paddingTop:10}}>
                        <Text>CARD 2</Text>
                    </TouchableOpacity>
            </Animated.View>
            <Animated.View style={{...styles.card3, transform: [{
                    translateY: card3ScrollY
                }]}}>
                    <TouchableOpacity onPress={() => this.cardTapped(3)} style={{backgroundColor:'transparent', alignItems:'center', width:'100%', height:'100%', paddingTop:10}}>
                        <Text>CARD 3</Text>
                    </TouchableOpacity>
            </Animated.View>

            <Transition
                openProgress={openProgress}
                photo={photo}
                sourceImageRefs={this._images}
                isAnimating={isAnimating}
                />
            <DetailScreen
                photo={photo}
                onClose={this.close}
                openProgress={openProgress}
                isAnimating={isAnimating}
                />
            {/* {this.state.isCardSelected && (
                <ExpandedCard
                    yOffset={this.state.yOffset}
                    xOffset={this.state.xOffset}
                    cardWidth={this.state.cardWidth}
                />
            )} */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    headerView: {
        height:100,
        width:'100%',
        // backgroundColor:'tra',
        padding:20,
        paddingTop:35
    },
    titleText:{
        color:'#000',
        fontSize:30,
        fontWeight:'500'
    },
    card1: {
        position: 'absolute',
        top: 100,
        left: 20,
        right: 20,
        height: DeviceHeight - 320,
        borderRadius: 10,
        backgroundColor: 'red'
    },
    card2: {
        position: 'absolute',
        top: 175,
        left: 20,
        right: 20,
        height: DeviceHeight - 320,
        borderRadius: 10,
        backgroundColor: 'purple'
    },
    card3: {
        position: 'absolute',
        top: 250,
        left: 20,
        right: 20,
        height: DeviceHeight - 320,
        borderRadius: 10,
        backgroundColor: 'orange'
    },
});


